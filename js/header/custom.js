$(function() {
    $('.search-toggle').click(function() {
        $('#modal-search').fadeToggle(300);
    })
    $('#dl-menu-studio').dlmenu({
        animationClasses: {
            classin: 'dl-animate-in-2',
            classout: 'dl-animate-out-2'
        }
    });
    $('#dl-menu').dlmenu({
        animationClasses: {
            classin: 'dl-animate-in-2',
            classout: 'dl-animate-out-2'
        }
    });
})